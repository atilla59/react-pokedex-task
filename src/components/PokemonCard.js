import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import styled from 'styled-components';
import './../style/pokemonCard.css'
import {FavoriteService} from "../services/FavoriteService";

const Sprite = styled.img`
  width: 6em;
  height: 6em;
`;

const Card = styled.div`
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);

  &:hover {
    box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  }
  -moz-user-select: none;
  -website-user-select: none;
  user-select: none;
  -o-user-select: none;
  border-radius: 10px;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: black;

  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;

const PokemonCard = ({pokemon, url,setFavoriteList}) => {
    const [sprite, setSprite] = useState('')
    const [pokeTypes, setPokeTypes] = useState([])

    const [isFavorite, setIsFavorite] = useState(false)

    const [loadImage, setLoadImage] = useState(true)
    const [tooManyRequests, setTooManyRequests] = useState(false)
    const [didMount, setDidMount] = useState(false)
    const [indexNum] = useState(url.split('/')[url.split('/').length - 2])

    useEffect(() => {
        axios.get('https://pokeapi.co/api/v2/pokemon/' + pokemon)
            .then(res => {
                setSprite(res.data.sprites.other.dream_world.front_default)
                setPokeTypes(res.data.types)
            })
        setDidMount(true);
        setIsFavorite(checkIsFavorite)
        return () => setDidMount(false)



    }, [pokemon])



    if (!didMount) {
        return null;
    }

    function addFavorite(e) {
        e.preventDefault();
        if (checkIsFavorite()){
            FavoriteService.remove({name:pokemon,url:url})
            setIsFavorite(false)
            if (setFavoriteList){
                setFavoriteList(FavoriteService.getList())
            }

        }else{
            FavoriteService.add({name:pokemon,url:url})
            setIsFavorite(true)
        }

    }

    function checkIsFavorite(){
        return FavoriteService.isFavorite({name:pokemon,url:url}) || isFavorite
    }

    return (
        <div className='col-md-3 col-sm-6 mb-5'>
            <StyledLink
                to={`pokemon/${indexNum}`}
                // target='_blank'
                // title={`#${indexNum}:` + pokemon}
            >
                <Card className='card'>
                    <div className='card-body'>

                        <div className="row">

                            <div className={"col-md-12 favorite " + ( isFavorite ? 'in-list' : '')} onClick={addFavorite}>

                                <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                    <path fillRule="evenodd"
                                          d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
                                          clipRule="evenodd"/>
                                </svg>
                            </div>

                            <div className="col-md-6">
                                <div className="row">
                                    <div className="col-md-12">
                                        <h6>
                                            {pokemon
                                                .toLowerCase()
                                                .split('-')
                                                .map(letter => letter.charAt(0).toUpperCase() + letter.substring(1))
                                                .join(' ')
                                            }
                                        </h6>
                                    </div>
                                    <div className="row">
                                        {pokeTypes.map((i) => (
                                            <div className="col-md-12 col-sm-12 poke-type" key={i.type.name}>
                                                {i.type.name}
                                            </div>
                                        ))}
                                    </div>


                                </div>
                            </div>
                            <div className="col-md-6">
                                <Sprite
                                    className='pokemon-img'
                                    src={sprite}
                                    alt='Pokémon Sprite'
                                    onLoad={() => setLoadImage(false)}
                                    onError={() => setTooManyRequests(true)}
                                    style={
                                        tooManyRequests ? {display: 'block'} :
                                            loadImage ? null : {display: 'block'}
                                    }
                                />
                            </div>
                        </div>


                    </div>
                </Card>
            </StyledLink>
        </div>
    )

}

export default PokemonCard
