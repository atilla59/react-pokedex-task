import React, { useState, useEffect } from 'react';
import PokemonCard from './PokemonCard';
import axios from 'axios';
import Pagination from "./Pagination";

const PokemonList = () => {
    const [pokemon, setPokemon] = useState([])
    const [currentPageUrl, setCurrentPageUrl] = useState('https://pokeapi.co/api/v2/pokemon')
    const [loading, setLoading] = useState(true)
    const [nextPageUrl, setNextPageUrl] = useState()
    const [prevPageUrl, setPrevPageUrl] = useState()

    useEffect(() => {
        setLoading(true)
        let cancel
        axios.get(currentPageUrl, {
            // to avoid a race condition
            cancelToken: new axios.CancelToken(c => cancel = c)
        }).then(res => {
            setLoading(false)
            // console.log(res.data.results)
            setPokemon(res.data.results)
            setNextPageUrl(res.data.next)
            setPrevPageUrl(res.data.previous)
        })

        return () => cancel()

        // every time currentPageUrl changes, this will trigger the useEffect hook and refresh the application
    }, [currentPageUrl])

    const goToNextPage = () => {
        setCurrentPageUrl(nextPageUrl)
    }

    const goToPrevPage = () => {
        setCurrentPageUrl(prevPageUrl)
    }

    if (loading) return "Loading Your Pokémon..."

    return (
        <>
            {pokemon ? (
                <div className='row'>
                    {pokemon.map((i) => (
                        <PokemonCard
                            key={i.name}
                            pokemon={i.name}
                            url={i.url}
                        />
                    ))}
                </div>
            ) : (
                <h1>Loading Pokemons</h1>
            )}
            <div className='row'>
                <Pagination
                    goToNextPage={nextPageUrl ? goToNextPage : null}
                    goToPrevPage={prevPageUrl ? goToPrevPage : null}
                />
            </div>
        </>
    )
}

export default PokemonList
