import React from 'react';
import PokemonList from "../components/PokemonList";
import {Link} from "react-router-dom";
import "./../style/dashboard.css"

const Dashboard = () => {
    return (
        <div className='row'>
            <div className="favorite-link">
                <Link to="/favorites">My Favorite List</Link>
            </div>
            <div
                className='col'
                style={{
                    marginTop: '25px'
                }}
            >
                <PokemonList />
            </div>

        </div>
    )
}
export default Dashboard
