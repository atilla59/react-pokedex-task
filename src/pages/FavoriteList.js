import React, {useEffect, useState} from 'react';
import PokemonCard from "../components/PokemonCard";
import {FavoriteService} from "../services/FavoriteService";
import {Link} from "react-router-dom";
import "./../style/favoriteList.css"


const FavoriteList = () => {

    const [favoriteList, setFavoriteList] = useState([])

    useEffect(() => {

        console.log(FavoriteService.getList())
        setFavoriteList(FavoriteService.getList())

    }, [])

    return (
        <div className='row'>
            <div className="col-md-12 pb-4">
                <div className="row">
                    <div className="col-md-6">Favorite List</div>
                    <div className="col-md-6 navs">
                        <Link  to="/">Pokemon List</Link>
                    </div>
                </div>
            </div>

            <div className="row">
                {favoriteList.map((poke) =>
                    (
                        <PokemonCard
                            key={poke.name}
                            pokemon={poke.name}
                            url={poke.url}
                            setFavoriteList={setFavoriteList}
                        />
                    )
                )}
            </div>

        </div>
    )
}
export default FavoriteList
