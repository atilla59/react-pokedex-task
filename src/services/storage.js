export const storage = {
    getValue(key){
        return JSON.parse(localStorage.getItem(key))
    },
    setValue(key,value){
        localStorage.setItem(key,JSON.stringify(value))
    }
}
