import {storage} from "./storage";

const storageKey="my-poke-list"
export const FavoriteService = {
    getList() {
        return storage.getValue(storageKey) || [];
    },
    add(poke) {
        let currentList = storage.getValue(storageKey) || [];
        currentList.push(poke);
        return storage.setValue(storageKey,currentList);
    },
    remove(poke) {
        let currentList = storage.getValue(storageKey) || [];
        let foundedIndex = currentList.findIndex((item)=>item.name ===poke.name)
        if (foundedIndex>-1){
            currentList.splice(foundedIndex,1)
        }
        return storage.setValue(storageKey,currentList)
    },
    isFavorite(poke){
        let currentList = storage.getValue(storageKey) || [];
        let foundedIndex = currentList.findIndex((item)=>item.name ===poke.name)
        return foundedIndex > -1
    }
}
