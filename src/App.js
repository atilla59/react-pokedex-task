
import './App.css';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Dashboard from "./pages/Dashboard";
import Pokemon from "./components/Pokemon";
import FavoriteList from "./pages/FavoriteList";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
      <Router>
        <div
            className='App'
            style={{
              backgroundColor: '#f5f5f5'
            }}
        >

          <div className='container'>
            <Switch>
              <Route
                  exact path='/'
                  component={Dashboard}
              />
              <Route
                  exact path='/pokemon/:indexNum'
                  render={(props) => <Pokemon {...props} />}
              />
                <Route
                    exact path='/favorites'
                    component={FavoriteList}
                />
            </Switch>
          </div>
        </div>
      </Router>
  );
}

export default App;
